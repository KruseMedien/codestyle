import chalk from 'chalk';
import clear from 'clear';
import figlet from 'figlet';

const header = title => {
    /* eslint-disable-next-line no-console */
    console.log(chalk.blue(figlet.textSync(title, 'Kban')));
};

const error = message => write(message, 'Error', chalk.red);

const success = message => write(message, 'Success', chalk.green);

const warn = message => write(message, 'Warning', chalk.yellow);

/* eslint-disable-next-line no-console */
const write = (message, type, color) => console.log(color(`[${type}]`), message);

export default {
    clear,
    error,
    header,
    success,
    warn,
};

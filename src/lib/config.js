import _ from 'lodash';

let config = {};

const defaults = {
    cwd: process.cwd(),
};

const get = path => _.get(_.defaultsDeep(config, defaults), path);

const reset = () => (config = {});

const set = (path, value) => _.set(config, path, value);

const unset = path => _.unset(config, path);

export default {
    get,
    reset,
    set,
    unset,
};

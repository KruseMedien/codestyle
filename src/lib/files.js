import _ from 'lodash';
import config from './config';
import fs from 'fs';
import git from 'simple-git/promise';
import path from 'path';

const isDirectory = dir => {
    try {
        return fs.statSync(dir).isDirectory();
    } catch (err) {
        return false;
    }
};

const copyInitial = () => {
    try {
        _.each(getInitial(), file => {
            if (cwd.exists(file) === false) {
                fs.copyFileSync(lib.path('initial/' + file), cwd.path(file));
            }
        });
        return true;
    } catch (err) {
        return false;
    }
};

const copyRelease = () => {
    try {
        _.each(getRelease(), file => {
            fs.copyFileSync(lib.path('release/' + file), cwd.path(file));
        });
        return true;
    } catch (err) {
        return false;
    }
};

const getInitial = () => {
    let files = fs.readdirSync(lib.path('initial'));
    files = _.filter(files, file => isDirectory(lib.path('initial/' + file)) === false);
    return files;
};

const getRelease = () => {
    let files = fs.readdirSync(lib.path('release'));
    files = _.filter(files, file => isDirectory(lib.path('release/' + file)) === false);
    return files;
};

const createDirectory = dir => {
    try {
        fs.mkdirSync(dir, { mode: 0o744, recursive: true });
        return isDirectory(dir);
    } catch (err) {
        return false;
    }
};

const cwd = {
    createDirectory: dir => createDirectory(cwd.path(dir)),
    exists: file => fs.existsSync(cwd.path(file)),
    isDirectory: dir => isDirectory(cwd.path(dir)),
    path: dir => path.resolve(config.get('cwd'), _.defaultTo(dir, '.')),
    readComposer: () => {
        try {
            const path = cwd.path('composer.json');
            return JSON.parse(fs.readFileSync(path));
        } catch (err) {
            return null;
        }
    },
    readPackage: () => {
        try {
            const path = cwd.path('package.json');
            return JSON.parse(fs.readFileSync(path));
        } catch (err) {
            return null;
        }
    },
    writeComposer: json => {
        try {
            const path = cwd.path('composer.json');
            fs.writeFileSync(path, JSON.stringify(json, null, 4));
        } catch (err) {
            return null;
        }
    },
    writePackage: json => {
        try {
            const path = cwd.path('package.json');
            fs.writeFileSync(path, JSON.stringify(json, null, 4));
        } catch (err) {
            return null;
        }
    },
};

const lib = {
    createDirectory: dir => createDirectory(lib.path(dir)),
    exists: file => fs.existsSync(cwd.path(file)),
    isDirectory: dir => isDirectory(lib.path(dir)),
    path: dir => path.resolve(__dirname, _.defaultTo(dir, '.')),
    readComposer: () => {
        try {
            const path = lib.path('initial/composer.json');
            return JSON.parse(fs.readFileSync(path));
        } catch (err) {
            return null;
        }
    },
};

const appendIgnore = async (file, comment) => {
    let result = false;
    const repo = git(cwd.path());
    if (_.isEmpty(await repo.checkIgnore(cwd.path('.eslintcache')))) {
        result = true;
        fs.appendFileSync(
            cwd.path('.gitignore'),
            '\n' + (_.isEmpty(comment) === false ? comment + '\n' : '') + file + '\n'
        );
    }
    if (result === true) {
        await repo.add(cwd.path('.gitignore'));
    }
    return result;
};

export default {
    appendIgnore,
    copyInitial,
    copyRelease,
    cwd,
    getInitial,
    getRelease,
    lib,
};

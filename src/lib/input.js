import inquirer from 'inquirer';

const promptCreateGit = () =>
    inquirer.prompt([
        {
            default: false,
            message: 'Create a new git repository',
            name: 'createGit',
            type: 'confirm',
        },
    ]);

export default {
    promptCreateGit,
};

import { description, husky, lintstaged, scripts, version } from '../../package';
import _ from 'lodash';
import config from './config';
import files from './files';
import git from 'simple-git/promise';
import input from './input';
import output from './output';

const createGit = async () => {
    if (files.cwd.createDirectory() === false) {
        output.error('Unable to create target directory');
        process.exit(1);
    }

    output.success('Create tagret directory');

    if (files.copyInitial() === false) {
        output.error('Unable to copy initial files');
        process.exit(1);
    }

    output.success('Copy initial files');

    try {
        const repo = git(files.cwd.path());
        await repo.init();
        _.each(files.getInitial(), async file => {
            await repo.add(files.cwd.path(file));
        });

        await repo.commit(['Initial commit', 'Contains repository configuration.']);

        output.success('Create initial commit');
    } catch (err) {
        output.error('Unable to initialize repository');
        process.exit(1);
    }

    output.success('Finish create git repository');
};

const initCodeStyle = async () => {
    try {
        const repo = git(files.cwd.path());

        if (files.copyInitial() === false || files.copyRelease() === false) {
            output.error('Unable to copy missing release files');
            process.exit(1);
        }

        output.success('Copy release files');

        _.each(files.getRelease(), async file => {
            await repo.add(files.cwd.path(file));
        });

        _.each(files.getInitial(), async file => {
            await repo.add(files.cwd.path(file));
        });

        if (await files.appendIgnore('.eslintcache', '# Cache files')) {
            output.success('Add ignored files');
        }

        let json = files.cwd.readPackage();

        _.defaultsDeep(json, {
            husky: husky,
            'lint-staged': lintstaged,
            scripts: _.omit(scripts, ['build', 'dev', 'precommit', 'watch']),
        });

        files.cwd.writePackage(json);

        if (files.cwd.exists('composer.json')) {
            json = files.cwd.readComposer();

            _.defaultsDeep(json, _.pick(files.lib.readComposer(), ['require-dev']));

            files.cwd.writeComposer(json);

            output.success('Add composer configuration');
        }

    } catch (err) {
        output.error('Unable to initialize code style');
        process.exit(1);
    }

    output.success('Finish initialize code style');
    output.success('Please check differences, run the linter/fixer and create a commit!');
};

const header = () => {
    output.clear();
    output.header('CodeStyle');
};

const init = async dir => {
    if (!_.isUndefined(dir)) {
        config.set('cwd', dir);
    }

    const answers = {};

    if (files.cwd.isDirectory('.git') === false) {
        _.extend(answers, await input.promptCreateGit());
        if (answers.createGit === false) {
            output.error('Operation cancelled');
            process.exit(1);
        }

        await createGit();
    }

    await initCodeStyle();
};

export default {
    description,
    header,
    init,
    output,
    version,
};

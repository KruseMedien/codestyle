#!/usr/bin/env node

import _ from 'lodash';
import commander from 'commander';
import lib from './lib';

lib.header();

commander.description(lib.description).version(lib.version, '-v, --version');

commander
    .command('init [dir]')
    .description('Initialize code style')
    .action(lib.init);

commander.parse(process.argv);

if (_.isEmpty(commander.args)) {
    commander.outputHelp();
}

# KruseMedien code style

## Install

First create a package.json if not exists:

```json
{
    "private": true
}
```

Now run the following command:

```cmd
npm install @krusemedien/codestyle --dev
```

## Initialisation

To initialize the project simple run the following command:

```cmd
./node_modules/.bin/codestyle init
```

After initialising the project run:

```cmd
composer install
```

## Code style check

To check the codestyle run the desired npm script:

```cmd
npm run lint
```

To run codesniffer and to display the codesniffer rule names add the -s argument:

```cmd
npm run lint:php . -- -s
```

### Help

If you want to set custom codesniffer or eslint rules, you may find more helpful examples:

-   [Codesniffer ruleset](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Annotated-Ruleset)
-   [Eslint configuration](https://eslint.org/docs/user-guide/configuring)

## Code style fix

To fix the codestyle run the desired npm script:

```cmd
npm run fix
```
